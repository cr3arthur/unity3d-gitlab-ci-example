#!/usr/bin/env bash

set -e

docker run \
  -e UNITY_SERIAL \
  -e TEST_PLATFORM \
  -e UNITY_EMAIL \
  -e UNITY_PASSWORD \
  -w /project/ \
  -v $UNITY_DIR:/project/ \
  $IMAGE_NAME \
  /bin/bash -c "/project/ci/before_script.sh && /project/ci/test.sh"
