#!/usr/bin/env bash

set -e

docker run \
  -e BUILD_NAME \
  -e UNITY_SERIAL \
  -e BUILD_TARGET \
  -e UNITY_EMAIL \
  -e UNITY_PASSWORD \
  -w /project/ \
  -v $UNITY_DIR:/project/ \
  $IMAGE_NAME \
  /bin/bash -c "/project/ci/before_script.sh && /project/ci/build.sh"
